/* Heroku: igcinfo-falckonv.herokuapp.com

Set to aifood.fun
Web-site for AI FooD
   Author Vanja Falck

*/

package main

import (
	"fmt"
"log"
	"net/http"
	"strings"
"os"
	"time"
	"encoding/json"
	//"regexp"
	// Web-framework
	"github.com/gin-gonic/gin"
	//"github.com/gopherjs/gopherjs/js"
	_ "github.com/heroku/x/hmetrics/onload"
)
/*
GlobalDb is a memory session database only. It is implemented as an interface
and can easily be exchanged with permanent storage.
*/
var GlobalDb FoodStorage

/*
FoodItem object has all information about the Nutrients records.
FoodItem is the key in the database which keep Nutrients objects.
*/
type FoodItem struct {
	Nutrients
	RecURL string `json:"url"`
	RecID  int    `json:"id"` // Unique /igcinfo/api/<id>/
}

/*
Nutrients object stores all flight Nutrients information.
Nutrients is embedded in the FoodItem object
*/
type Nutrients struct {
	HDate       string  `json:"H_date"`       // Date Header H-record
	Pilot       string  `json:"pilot"`        // Pilots name
	Glider      string  `json:"glider"`       // Glider Type
	GliderID    string  `json:"glider_id"`    // Glider ID
	TrackLength float64 `json:"track_length"` // Calculated length (km)
}

/*
Points is an object for holding and computing values of food items.
TODO: Implement
*/
type Points struct {

}

/*
ServiceInfo is an object containing package information and
uptime for the web-service.
TODO: Implement
*/
type ServiceInfo struct {
	Uptime  string `json:"uptime"`
	Info    string `json:"info"`
	Version string `json:"version"`
}

/*
FoodStorage is an interface for treating all data of food items.
*/
type FoodStorage interface {
	Init()
	Add(reg FoodItem) error
	Count() int
	GetFoodItem(reg FoodItem) (FoodItem, bool)
	GetAll() []FoodItem
	ImportJSONFood(u string) (FoodItem, bool)
	APIFields(ap string) bool
	//NewID() (string)
}

/*
FoodDB takes the FoodItem object as key for maps
keeping easy look up
TODO maybe this is redundant
*/
type FoodDB struct {
	// Database for metadata for flight Nutrients records
	nutrients  map[FoodItem]Nutrients
	// Look-up map for simplified checking of existing
	// urls in database and allocating of new ids
	urlkeys map[string]int
	// Map of API elements for checking incoming text
	fields  map[string]int
}

/*
Init initiates an empty FoodDB database with
session memory storage as a map with FoodItem as key
TODO: Change to appropriate key words
*/
func (db *FoodDB) Init() {
	// db.nutrients is the map-database for Nutrients data
	db.nutrients  = make(map[FoodItem]Nutrients)
	// db.urlkeys store urls as keys for easy look-up of ids in a map
	db.urlkeys = make(map[string]int)
	// db.fields HARDCODED of valid API words (used instead of regexp check)
	// TODO: Change to appropriate words for food items
	db.fields  = map[string]int{"pilot":1,"glider":2,"glider_id":3,"track_length":4,"h_date":5}
	/*
	HARDCODED TEST input to database
		s1 := FoodItem{RecID: 1, RecURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc", Track: Track{HDate: "2016-10-05", Pilot: "Siv Toppers", Glider: "Mypmyp", GliderID: "AIKK-3", TrackLength: 764}}
		s2 := FoodItem{RecID: 2, RecURL: "http://example.com/igcinfo/api/igc/track2", Track: Track{HDate: "2015-11-10", Pilot: "Vanja Falck", Glider: "Ompa", GliderID: "AIKK-5", TrackLength: 223}}
		s3 := FoodItem{RecID: 3, RecURL: "http://example.com/igc/track3", Track: Track{HDate: "2017-04-09", Pilot: "Marius Muller", Glider: "Theodor", GliderID: "AIKK-12", TrackLength: 346}}
		db.nutrients[s1] = s1.Track
		db.nutrients[s2] = s2.Track
		db.nutrients[s3] = s3.Track
		db.urlkeys[s1.RecURL] = 1
		db.urlkeys[s2.RecURL] = 2
		db.urlkeys[s3.RecURL] = 3
	*/
}

/*
APIFields check for a matching name in the db.fields database.
*/
func (db *FoodDB) APIFields(ap string) bool {
  _, ok := db.fields[ap]
	 if !ok {
		 return false
	 }
return true
}

/*
ImportJSONFood takes an url string as argument and check if
it is a valid .json food record file.
If valid json record:
The content is extracted. The Nutrients is stored as a FoodItem object
with url and a recordID with Tracks (metadata).
TODO: Refractor Goigc/igc parsing to take a json-file.
This function returns an empty FoodItem if false. Should
find a better way!
*/
func (db *FoodDB) ImportJSONFood(u string) (FoodItem, bool) {
// Parsing a igc file from url u
// < Refractor goigc-module (removed) here>
var reg FoodItem
reg.RecID = 0
// Dummy
reg.RecURL = "http://www.nrk.no"
return reg, false
}

// Add FoodItem objects to the database
func (db *FoodDB) Add(reg FoodItem) error {
	db.nutrients[reg] = reg.Nutrients
	return nil
}

// Count the number of objects in the database
func (db *FoodDB) Count() int {
	return len(db.nutrients)
}



/*
GetFoodItem takes a FoodItem (key in database) if
match on either the url or the id of the object, a
complete FoodItem object with flight records is returned.
If FoodItem object is not complete - false, else true.
TODO better handling of errors - may be restructure return values
Maybe make a function isInDB() to check if a reg is present and
make error handling based on this (as this checking is done several times)
Fetch Nutrients object with key == tr
If Nutrients exist:
Return the FoodItem object with id/url and track
*/
func (db *FoodDB) GetFoodItem(reg FoodItem) (FoodItem, bool) {

switch {
	//Get Nutrients data directly from DATABASE
  case reg.RecID > 0 && reg.RecURL != "":
	// Checks if reg is in database:
	_ , ok := db.nutrients[reg]
	if !ok {
		return reg, false
	}
	return reg, true

	// Get Nutrients data from GET api/<track-00xx> by id
	// To display metadata and separate fields
  case reg.RecURL == "":
	// Loops the database to check based on the Nutrients id
	if reg.RecID > 0 {
		var key FoodItem
		for k, trc := range db.nutrients {
			if trc == db.nutrients[k] {
				key = k
				if key.RecID == reg.RecID {
					return key, true
				}
			}
		 }
	 }
	// Get Nutrients data from POST api/igc
	// To transform by goigc/igc package and
	// stored in database and displayed
  case reg.RecURL != "":
	if reg.RecID == 0 {
	// Get the Nutrients id from db.urlkeys
	numID    := db.urlkeys[reg.RecURL]
	reg.RecID  = numID
	reg.Nutrients = db.nutrients[reg]
	return reg, true
  }
 }
 // IF no option in switch - the checked reg cannot be confirmed
return reg, false
}

/*
GetAll returns all current FoodItem objects (the keys
in the database).
*/
func (db *FoodDB) GetAll() []FoodItem {
	// Makes a slice at the size of db
	gAll := make([]FoodItem, 0, db.Count())

	// List all current FoodItem objects in an array
	for k := range db.nutrients {
		gAll = append(gAll, k)
	}
	return gAll
}

/*
setAllFoodItems display JSON api/nutrients
---> array of all registered Nutrients ids
*/
func setAllFoodItems(w http.ResponseWriter, db FoodStorage) {
  if db.Count() == 0 {
		json.NewEncoder(w).Encode([]FoodItem{})
	} else {
		add := make([]FoodItem, 0, db.Count())
		for _, reg := range db.GetAll() {
			add = append(add, reg)
		}
		json.NewEncoder(w).Encode(add)
	}
}

/*
setOneFoodItem display JSON object (unnested) displayed
---> The query for <id> identified by the split url response "u"
*/
func setOneFoodItem(w http.ResponseWriter, db FoodStorage, u string) {
  // Find the reg in db with FoodItem.Id == u (string)
  // Trenger egentlig bare return VALUE (=id) her...
	var dummy FoodItem
	dummy.RecURL = u
  reg, ok := db.GetFoodItem(dummy)
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}
	json.NewEncoder(w).Encode(reg)
}

/*
HandlerFoodItem perform the POST function for food records
TODO: change this to fit the router Gin framework
TODO: change to fit an incomming food record (need to change objects)
*/
func HandlerFoodItem(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		var reg FoodItem
		err := json.NewDecoder(r.Body).Decode(&reg)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
      return
		}
		_, ok := GlobalDb.GetFoodItem(reg)
		if ok {
			// TODO Better ERROR HANDLING?
			http.Error(w, "This Record id is already registered.", http.StatusBadRequest)
      return
		}
		GlobalDb.Add(reg)
		fmt.Fprint(w, "Successfully registered track")
		return
	case "GET":
		http.Header.Add(w.Header(), "content-type", "application/json")
		// Can also use:
		// w.Header().Add("content-type", "application/json")
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) != 3 || parts[1] != "api" {
			http.Error(w, "Are you sure you are at the extension /api?", http.StatusBadRequest)
			return
		}
		if parts[2] == "" {
			setAllFoodItems(w, GlobalDb)
		} else {
      // NB denne må endres fordi input i siste ledd er TrackReg object
      // og ikke id string som i STUDENTDB - her hentes recordID´en
			setOneFoodItem(w, GlobalDb, parts[2])
		}
	default:
		http.Error(w, "This is not an option in this API.", http.StatusBadRequest)
		return
	}
}

/*
PostURL get data by POST

TODO:
// Getting keys from DB as a type interface:
db, ok := context.Keys["DB"].(*DB)
if !ok {
        //Handle case of no *DB instance
}
// db is now a *DB value

TODO SHOULD INCLUDE - as maps are not concurrent
SafeNutrients sync.Mutex 		// Protects session
Maxlifetime int64				    // Session duration
*/
func PostURL() gin.HandlerFunc {
    return func(c *gin.Context) {
      // Drop authorisation
      // pass, err := SERVER.C(c).GetAuthPass(models.AuthRequest{
		var RecURL string
    c.Request.Header.Get(RecURL)
		/*
    UDID:      c.Request.Header.Get("X-Auth-UDID"),
    })
        if err != nil {
            c.Fail(401, err)
        } else {
            // Map authpass to the current http request
            c.Set("auth", pass)
        }
			*/
    }
}


func main() {

// Session based memory
GlobalDb = &FoodDB{}
// UPTIME MEASUREMENT ---------
// .Format(time.RFC3339)

// GLOBAL PORT ----------------
port := os.Getenv("PORT")

	if port == "" {
	log.Fatal("$PORT must be set")
	}

// GIN ROUTER to handle patterns for serverside urls
router := gin.New()
// Logging
router.Use(gin.Logger())
// Load all html-templates from the template dir
router.LoadHTMLGlob("templates/*.tmpl.html")
// Points to the static dir content (css images etc)
router.Static("/static", "static")

// MAIN PAGE
router.GET("/aifood/", func(c *gin.Context) {
	  c.Header("Date", time.RFC3339)
		c.HTML(http.StatusOK, "front.tmpl.html", gin.H{
					"title": "AIFooD",
				})
	})

router.GET("/aifood/info/", func(c *gin.Context) {
		 c.Header("Date", time.RFC3339)
		c.HTML(http.StatusOK, "info.tmpl.html", gin.H{
						"title": "AIFooD API Information",
					})
	})

// API with gin
// https://medium.com/@thedevsaddam/build-restful-api-service-in-golang-using-gin-gonic-framework-85b1a6e176f3

router.GET(("/aifood/api/nutrients/"), func(c *gin.Context) {
	  // Using gin to change header settings and information:
	  c.Header("Content-Type", "application/json")
		// Timer is not tested
		//c.Header("Uptime", (t(&start, &step)).String())
    c.Header("Date", time.RFC3339)
		//This is working:
    c.IndentedJSON(http.StatusOK, GlobalDb)
	})

/*
router.POST("/aifood/api/", func(c *gin.Context) {
	  var urlTrack string
		// Using gin to change header settings and information:
	  c.Header("Content-Type", "application/json")
		// This is not completely correct
		//c.Header("Uptime", (t(&start, &step).String()))
		c.Header("Date", time.RFC3339)
		c.HTML(http.StatusOK, "submit.tmpl.html", nil)
    var urlNutrients string
		c.PostForm(urlNutrients)
		// Tested regexp in http://regexr.com
		pattern := `^https?:\/\/[^\s$.?#].[^\s]*json$`
		res, err := regexp.MatchString(pattern, urlTrack)
		if err != nil {
				c.Error(err)
		}

		Nutrients := GlobalDb.ImportJSONFood(urlTrack)
		c.IndentedJSON(http.StatusOK, track)

		if res {
				// Function returns a FoodItem object
				Nutrients := GlobalDb.ImportJSONFood(urlTrack)
				// TODO Errorhandling
				c.IndentedJSON(200, track)

				c.IndentedJSON(200, gin.H {
					"status":  "Your Nutrients was successfully posted",
					// Get the new recordID:
					"recordID": track.RecID,
					})
		}

		//Not working within gin contex
		//response, error := http.Get(urlTrack)
		// REQUIRED: handle error
		if error != nil {
			fmt.Println("This is not working - url cannot be translated to json", error)
		}
	})

	/*

/*
		// REQUIRED: Close AFTER the body is retrieved
		defer response.Body.Close()
		defer c.Abort()
		c.HTML(http.StatusOK, "json.tmpl.html", nil)
		router.GET("/igcinfo/api", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl.html", nil)

		Nutrients := GlobalDb.ImportJSONFood(urlTrack)
		// REQUIRED: handle error
		fmt.Println("This is not working - url cannot be stored", error)

		// Publish the transformed igc file as a FoodItem JSON object
		c.IndentedJSON(http.StatusOK, track)

		// DENNE VIRKER og tar imot data fra Postman
		message := c.PostForm("Your new Nutrients is at /aifood/api/" + foodrecord.ID)
	})


	pathNutrients := "/aifood/api/nutrients"
	router.GET(pathNutrients, func(c *gin.Context) {
		c.Header("Content-Type", "application/json")
		c.Header("Date", time.RFC3339)
	})
*/

/*
POST /api/
(the same /api/ should also have a GET request to recieve an ARRAY
of all id´s stored on the site)

What: Nutrients registration
Response type: application/json
Response code: 200 if everything is OK, appropriate error code otherwise, eg.
when provided body content, is malformed or URL does not point to a proper
IGC file, etc. Handle all errors gracefully.

Request body template
{
  "url": "<url>"
}
Response body template
{
  "id": "<id>"
}
where: <url> represents a normal URL, that would work in a browser, eg:
http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc
and <id> represents an ID of the track, according to your internal management
system. You can choose what format  should be in your system. The only
restriction is that it needs to be easily used in URLs and it must be unique.
It is used in subsequent API calls to uniquely identify a track, see below.

TODO: make regex expression to call on all 404:
Gets the 404-template for the root directory with default handler (nil)

nil can be replaced with
, gin.H{
			"title": "Main website",
		})
var regx = `^\/[a-z]{7}\/[a-z]{3}\/([a-z]{3}\/[a-zA-Zå-øÅ-Ø]{5}[0-9]{1}*$`

*/

	router.GET("/aifood/api/explore", func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "404.tmpl.html", nil)
	})
	router.GET("/aifood/api/food", func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "404.tmpl.html", nil)
	})
	router.GET("/api/", func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "404.tmpl.html", nil)
	})

	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "404.tmpl.html", nil)
	})

	//router.Run("127.0.0.1:" + port)
  router.Run(":" + port)
}
