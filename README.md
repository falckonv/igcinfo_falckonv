# igcinfo_falckonv


NOTES: Still have problems with heroku to accept local changes. Regret painfully having launched a regular set up with index.html as heroku persistenly refuses to load these changes. If these issues are impossible to solve, I will deploy on a new heroku instance.



An igcinfo app for IMT2681 autumn 2018. First assignment.

Heroku-deployment on: https://igcinfo-falckonv.herokuapp.com

Task:

Develop an online service that will allow users to browse information about IGC files. IGC is an international file format for soaring Nutrients files that are used by paragliders and gliders. The program will not store anything in a persistent storage. Ie. no information will be stored on the server side on a disk or database. Instead, it will store submitted tracks in memory. Subsequent API calls will allow the user to browse and inspect stored IGC files.

For the development of the IGC processing, you will use an open source IGC library for Go: goigc

The system must be deployed on either Heroku or Google App Engine, and the Go source code must be available for inspection by the teaching staff (read-only access is sufficient).
